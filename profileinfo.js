/*
	pikabu.ru additional profile info
	Copyright (C) 2014 Denis Vesnin, http://vesn.in/git

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
	documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
	the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
	CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
	DEALINGS IN THE SOFTWARE.
*/

function PikabuProfileInfo() {
	var self = this;

	self.rootUrl = "";					// for debug. Empty for production
	self.cacheKeySuffix = "_infocache";
	self.myName = $("td.name>h4>a")[0].innerText;	// capture my profile name from document page
	self.profileRoleThreshold = 0.3;			// good vs evil ratio threshold (see getProfileRoleValue)
	self.cacheTtlSeconds = 60*60*2;
	self.isLocalStorageAvailable = (function() {
		try {
			return "localStorage" in window && window["localStorage"] !== null;
		} catch (e) {
			return false;
	  	}
	})();

	self.getProfileKey = function(profileName) {
		return "profile_" + profileName;
	}

	self.getProfileUrl = function(profileName) {
		return self.rootUrl + "/profile/" + profileName;	// need to check this scenario!
	}

	self.tryStoreProfileInfo = function(profileInfo) {
		if(!self.isLocalStorageAvailable)
			return false;

		localStorage.setItem(profileInfo.profileName + self.cacheKeySuffix, JSON.stringify(profileInfo));
	}

	self.tryRestoreProfileInfo = function(profileName) {
		if(!self.isLocalStorageAvailable)
			return false;

		var info = localStorage.getItem(profileName + self.cacheKeySuffix);
		if(!info)
			return false;

		info = JSON.parse(info);
		info.lastUpdate = new Date(info.lastUpdate);

		return info;
	}

	self.isProfileOutdated = function(profileInfo) {
		var seconds = new Date() - profileInfo.lastUpdate;
		return seconds > self.cacheTtlSeconds;
	}

	self.loadProfileInfo = function(profileName, onLoad) {
		var profileUrl = self.getProfileUrl(profileName);
		$.get(profileUrl, function(data) {
			var wrap = $("div.profile_wrap", data)[0].innerText;

			var totalWord = wrap.match(/пикабушни.*уже.*/)[0].match(/\d.*/); 
			totalWord = totalWord ? totalWord[0] : "";
			var y = totalWord.match(/\d+(?=\s(г|л))/); y = y ? (y[0]*1) : 0;
			var m = totalWord.match(/\d+(?=\sм)/); m = m ? (m[0]*1) : 0;
			var w = totalWord.match(/\d+(?=\sн)/); w = w ? (w[0]*1) : 0;
			var d = totalWord.match(/\d+(?=\sд)/); d = d ? (d[0]*1) : 0;
			var totalDays = y*365 + m*30 + w*7 + d;

			var carma=wrap.match(/рейтинг.*/)[0].match(/\d+/)[0];

			var pluses = wrap.match(/\d+(?=\sплюс)/)[0]*1;
			var minuses = wrap.match(/\d+(?=\sминус)/)[0]*1;
			var role = self.formatProfileRole(self.getProfileRoleValue(pluses, minuses));

			var info = {
				profileName: profileName,
				profileUrl: profileUrl,
				age: totalDays,
				carma: carma,
				role: role,
				lastUpdate: new Date()
			};

			onLoad(info);
		});
	}

	self.getProfileRoleValue = function(pluses, minuses) {
		var roleValue = 0;

		if(pluses > minuses) {
			if(minuses == 0) {
				roleValue = 1;
			} else {
				roleValue += pluses/minuses;
			}
		} else if(minuses > pluses) {
			if(pluses == 0) {
				roleValue = -1;
			} else {
				roleValue -= minuses/pluses;
			}
		} else {
			roleValue = 0;
		}

		return Math.abs(roleValue) > self.profileRoleThreshold ? roleValue : 0;
	}

	self.formatProfileRole = function(roleValue) {
		if(roleValue == 0) {
			return "нейтрал";
		}
		var roleName = roleValue > 0 ? "добро" : "зло";
		return Math.abs(roleValue) == 1 ? ("абсолютное " + roleName) : roleName;
	}

	self.getProfileInfoAsync = function(profileName, callback) {
		var profileInfo = self.tryRestoreProfileInfo(profileName);

		if(!profileInfo) {
			self.loadProfileInfo(profileName, function(info) {
				self.tryStoreProfileInfo(info);
				callback(info)
			});
		} else {
			callback(profileInfo);
		}

	}

	self.appendProfileLinkInfo = function(link) {
		var profileName = link.href.match(/\/profile\/.*/)[0].replace("/profile/","");

		if(profileName == self.myName || link.innerText != profileName)	// decoration links and my profile
			return;

		self.getProfileInfoAsync(profileName, function(info) {
			link.innerHTML += " " + self.formatExtraInfo(info);
		});
	}

	self.formatExtraInfo = function(info) {
		return "(" + info.role + ", дней: " + info.age + ", карма: " + info.carma + ")";
	}

	self.modifyProfileLinks = function() {
		var profile_links=$("a[href*='/profile/']");
		$.each(profile_links, function(index, link) {
			self.appendProfileLinkInfo(link);
		});
	}
};

_pikabuProfileInfo = new PikabuProfileInfo();
_pikabuProfileInfoOnLoadExecuted = false;

function pikabuPluginProfileInfoOnLoad() {
	if(_pikabuProfileInfoOnLoadExecuted)
		return;

	_pikabuProfileInfoOnLoadExecuted = true;
	pikabuPluginProfileInfoExec();
}

function pikabuPluginProfileInfoExec() {
	_pikabuProfileInfo.modifyProfileLinks();
}

$(document).ready(function() {
	pikabuPluginProfileInfoOnLoad()
});

setTimeout(pikabuPluginProfileInfoOnLoad, 1000*5);	// try to partial update page even if document is not done
setInterval(pikabuPluginProfileInfoOnLoad, 1000*30);	// fix this shit, use injection!
